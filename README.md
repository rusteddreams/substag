# Basic {{tag}} substitution engine.

Requirements:

 - c++14
 - boost (header-only libs: spirit::qi and BOOST_THROW_EXCEPTION)

## Basic usage

	rendered_text = substag::render( your_template, your_tags, your_sections [,context] )

 - **your_template** can be std::string or any other container
 - **your_tags** tagname-to-tagvalue map, any map-like type can be used
 - **your_sections** sectionname-to-contextprefix map, any multimap-like type can be used
  (or map-like if you don't need multiple elements in sections)
 - **context** optional context prefix to prepend tag names before lookup, same way
  as used in section blocks

It will throw std::runtime_error on errors (missing closing tag in template, too deep 
recursion due too infinite loop in recursive tags expansion).

In some ways this engine is similar to Mustache or CTemplate templates, but there are 
few differences: 

 - it uses simple maps of strings as input data instead of complex variant-based hierarchies
 - all tags can be recursive, just like partials in Mustache
 - no html or any other kind of escaping
 - tag delimiter changes can't 'leak' out of section block

## Tag types

1. **Regular values**

	A {{name}} tag will be replaced by a value of a "name" key from tag_map.
	Can be recursive (just like partials in Mustache):

		"Hello world!" == substag::render( "Hello {{who}}"s, 
			map<string,string>{ {"who", "world{{excl}}"}, {"excl", "!"} }, 
			multimap<string,string>{} )

	Missing tags will be replaced by empty strings:

		"Hi!" == substag::render( "Hi{{missing}}!"s, map<string,string>{}, multimap<string,string>{} )

2. **Sections**

	Use it to repeat blocks of text multiple times or only if some tag exists in tag_map.
	{{#section}} begins section block, {{/section}} ends it. It will get all values with
	a "section" key form section_map and use it's values as context prefixes for block
	expansion:

		"Times: 07:23, 22:47, " == substag::render( "Times: {{#times}}{{hh}}:{{mm}}, {{/times}}"s, 
			map<string,string>{ {"t1hh", "07"}, {"t1mm", "23"}, {"t2hh", "22"}, {"t2mm", "47"} }, 
			multimap<string,string>{ {"times", "t1"}, {"times", "t2"} } )

	If value can't be found in section context it will be searched in outer context:

		"local/5, global/7, " == substag::render( "{{#list}}{{name}}/{{value}}, {{/list}}"s, 
			map<string,string>{ {"name", "global"}, {"1name", "local"}, {"1value", "5"}, {"2value", "7"} }, 
			multimap<string,string>{ {"list", "1"}, {"list", "2"} } )

	Check if regular tag exists:

		"warning (note: not serious)!" == substag::render( "warning{{#note}} (note: {{note}}){{/note}}!"s, 
			map<string,string>{ {"note", "not serious"} }, multimap<string,string>{} )

	Sections can be nested:

		"(11, 12), (21, 22, 23), " == substag::render( "{{#list}}({{#array}}{{i}}{{,}}{{/array}}), {{/list}}"s, 
			map<string,string>{ 
				{"1Ai", "11"}, {"1A,",", "}, {"1Bi", "12"}, 
				{"2Ai", "21"}, {"2A,",", "}, {"2Bi", "22"}, {"2B,",", "}, {"2Ci", "23"} }, 
			multimap<string,string>{ 
				{"list", "1"}, {"list", "2"}, 
				{"1array", "A"}, {"1array", "B"}, 
				{"2array", "A"}, {"2array", "B"}, {"2array", "C"} } )

3. **Inverted sections**

	{{^block}} begins inverted section block, {{/block}} ends it.
	Output block of text only if a tag or section is absent in tag_map and section_map:

		"Title: not provided" == substag::render( "Title: {{title}}{{^title}}not provided{{/title}}"s, 
			map<string,string>{}, multimap<string,string>{} )

4. **Comments**

	{{! comment goes here }} ignored and won't appear in output:

		"Hi!" == substag::render( "Hi{{! this will be ignored }}!"s, 
			map<string,string>{}, multimap<string,string>{} )

5. **Change tag delimiters**

		"A  {{tag}}=A  A" == substag::render( "{{tag}} {{=<+< >+>=}} {{tag}}=<+<tag>+> <+<={{ }}=>+> {{tag}}"s, 
			map<string,string>{ {"tag", "A"} }, multimap<string,string>{} )

	It can be used inside sections ("{{#list}}{{=[ ]=}}[i], [={{ }}=]{{/list}}"). But do not try to 'leak' 
	it out of section block ("{{#a}}{{=[ ]=}}[a]{{/a}}" or "{{#a}}{{=[ ]=}}[a][/a]" are bad).
