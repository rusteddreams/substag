#pragma once

/*
					 Copyright Ryhor Spivak 2016.
	Distributed under the Boost Software License, Version 1.0.
		(See accompanying file LICENSE_1_0.txt or copy at
				http://www.boost.org/LICENSE_1_0.txt)

# Basic {{tag}} substitution engine.

Requirements:

 - c++14
 - boost (header-only libs: spirit::qi and BOOST_THROW_EXCEPTION)

## Basic usage

	rendered_text = substag::render( your_template, your_tags, your_sections [,context] )

 - **your_template** can be std::string or any other container
 - **your_tags** tagname-to-tagvalue map, any map-like type can be used
 - **your_sections** sectionname-to-contextprefix map, any multimap-like type can be used
  (or map-like if you don't need multiple elements in sections)
 - **context** optional context prefix to prepend tag names before lookup, same way
  as used in section blocks

It will throw std::runtime_error on errors (missing closing tag in template, too deep 
recursion due too infinite loop in recursive tags expansion).

In some ways this engine is similar to Mustache or CTemplate templates, but there are 
few differences: 

 - it uses simple maps of strings as input data instead of complex variant-based hierarchies
 - all tags can be recursive, just like partials in Mustache
 - no html or any other kind of escaping
 - tag delimiter changes can't 'leak' out of section block

## Tag types

1. **Regular values**

	A {{name}} tag will be replaced by a value of a "name" key from tag_map.
	Can be recursive (just like partials in Mustache):

		"Hello world!" == substag::render( "Hello {{who}}"s, 
			map<string,string>{ {"who", "world{{excl}}"}, {"excl", "!"} }, 
			multimap<string,string>{} )

	Missing tags will be replaced by empty strings:

		"Hi!" == substag::render( "Hi{{missing}}!"s, map<string,string>{}, multimap<string,string>{} )

2. **Sections**

	Use it to repeat blocks of text multiple times or only if some tag exists in tag_map.
	{{#section}} begins section block, {{/section}} ends it. It will get all values with
	a "section" key form section_map and use it's values as context prefixes for block
	expansion:

		"Times: 07:23, 22:47, " == substag::render( "Times: {{#times}}{{hh}}:{{mm}}, {{/times}}"s, 
			map<string,string>{ {"t1hh", "07"}, {"t1mm", "23"}, {"t2hh", "22"}, {"t2mm", "47"} }, 
			multimap<string,string>{ {"times", "t1"}, {"times", "t2"} } )

	If value can't be found in section context it will be searched in outer context:

		"local/5, global/7, " == substag::render( "{{#list}}{{name}}/{{value}}, {{/list}}"s, 
			map<string,string>{ {"name", "global"}, {"1name", "local"}, {"1value", "5"}, {"2value", "7"} }, 
			multimap<string,string>{ {"list", "1"}, {"list", "2"} } )

	Check if regular tag exists:

		"warning (note: not serious)!" == substag::render( "warning{{#note}} (note: {{note}}){{/note}}!"s, 
			map<string,string>{ {"note", "not serious"} }, multimap<string,string>{} )

	Sections can be nested:

		"(11, 12), (21, 22, 23), " == substag::render( "{{#list}}({{#array}}{{i}}{{,}}{{/array}}), {{/list}}"s, 
			map<string,string>{ 
				{"1Ai", "11"}, {"1A,",", "}, {"1Bi", "12"}, 
				{"2Ai", "21"}, {"2A,",", "}, {"2Bi", "22"}, {"2B,",", "}, {"2Ci", "23"} }, 
			multimap<string,string>{ 
				{"list", "1"}, {"list", "2"}, 
				{"1array", "A"}, {"1array", "B"}, 
				{"2array", "A"}, {"2array", "B"}, {"2array", "C"} } )

3. **Inverted sections**

	{{^block}} begins inverted section block, {{/block}} ends it.
	Output block of text only if a tag or section is absent in tag_map and section_map:

		"Title: not provided" == substag::render( "Title: {{title}}{{^title}}not provided{{/title}}"s, 
			map<string,string>{}, multimap<string,string>{} )

4. **Comments**

	{{! comment goes here }} ignored and won't appear in output:

		"Hi!" == substag::render( "Hi{{! this will be ignored }}!"s, 
			map<string,string>{}, multimap<string,string>{} )

5. **Change tag delimiters**

		"A  {{tag}}=A  A" == substag::render( "{{tag}} {{=<+< >+>=}} {{tag}}=<+<tag>+> <+<={{ }}=>+> {{tag}}"s, 
			map<string,string>{ {"tag", "A"} }, multimap<string,string>{} )

	It can be used inside sections ("{{#list}}{{=[ ]=}}[i], [={{ }}=]{{/list}}"). But do not try to 'leak' 
	it out of section block ("{{#a}}{{=[ ]=}}[a]{{/a}}" or "{{#a}}{{=[ ]=}}[a][/a]" are bad).
*/

#ifndef SUBSTAG_RECURSION_LIMIT
#define SUBSTAG_RECURSION_LIMIT 64	// max tag expansion recursion depth
#endif

#include <string>
#include <vector>
#include <iterator>

#include <boost/fusion/include/adapt_struct.hpp>
#pragma warning( push )
#pragma warning( disable : 4127 )
#pragma warning( disable : 4100 )
#pragma warning( disable : 4239 )
#pragma warning( disable : 4459 )
#include <boost/spirit/home/x3.hpp>
#pragma warning( pop )

#include <boost/throw_exception.hpp>

namespace substag
{
	namespace detail
	{
		struct start_stop_markers
		{
			std::string start_, stop_;
		};
	}
}

BOOST_FUSION_ADAPT_STRUCT(
	substag::detail::start_stop_markers,
	(std::string, start_)
	(std::string, stop_)
)

namespace substag
{
	namespace detail
	{
		template<typename t_tag_map, typename t_section_map>
		struct substitution_contex
		{
			t_tag_map const & tag_map_;
			t_section_map const & section_map_;
			std::vector<std::string> context_prefixes_;
			std::string start_mark_, stop_mark_, result_;

			auto find_section_range( std::string const tag ) -> decltype(section_map_.equal_range( tag ))
			{
				for( auto i = context_prefixes_.rbegin(), end = context_prefixes_.rend(); i != end; ++i )
				{
					auto r = section_map_.equal_range( *i + tag );
					if( r.first != r.second )
						return r;
				}
				return section_map_.equal_range( tag );
			}

			auto find_section( std::string const tag ) -> decltype(section_map_.find( tag ))
			{
				for( auto i = context_prefixes_.rbegin(), end = context_prefixes_.rend(); i != end; ++i )
				{
					auto s = section_map_.find( *i + tag );
					if( s != section_map_.end() )
						return s;
				}
				return section_map_.find( tag );
			}

			auto find_tag( std::string const tag ) -> decltype(tag_map_.find( tag ))
			{
				for( auto i = context_prefixes_.rbegin(), end = context_prefixes_.rend(); i != end; ++i )
				{
					auto s = tag_map_.find( *i + tag );
					if( s != tag_map_.end() )
						return s;
				}
				return tag_map_.find( tag );
			}

			template<typename t_iter>
			void render( t_iter i, t_iter const end, int recursion_limit = SUBSTAG_RECURSION_LIMIT )
			{
				if( recursion_limit == 0 )
					BOOST_THROW_EXCEPTION( std::runtime_error( "Possible infinite loop while expanding \"" + std::string( i, end ) + '"' ) );

				namespace x3 = boost::spirit::x3;
				auto const ws = x3::lit(' ')|'\t';

				while( i != end )
				{
					std::string tagname;
					start_stop_markers new_start_stop;

					if( x3::parse( i, end,
						start_mark_ >> *ws >> '!' >> *(x3::char_-stop_mark_) >> stop_mark_ ) )
					{
						// skipping comment
					}
					else if( x3::parse( i, end,
						start_mark_ >> *ws >> '=' >> *ws >> +(x3::char_-(ws|'='|stop_mark_)) >> +ws >> +(x3::char_-(ws|'='|stop_mark_)) >> *ws >> '=' >> *ws >> stop_mark_,
						new_start_stop ) )
					{
						start_mark_ = new_start_stop.start_;
						stop_mark_ = new_start_stop.stop_;
					}
					else if( x3::parse( i, end,
						start_mark_ >> *ws >> '#' >> *ws >> +(x3::char_-(ws|stop_mark_)) >> *ws >> stop_mark_,
						tagname ) )
					{
						auto const section_begin = i;
						while( i != end )
						{
							auto const section_end = i;
							if( x3::parse( i, end,
								start_mark_ >> *ws >> '/' >> *ws >> tagname >> *ws >> stop_mark_ ) )
							{
								auto const section_range = find_section_range( tagname );
								for( auto isection = section_range.first; isection != section_range.second; ++isection )
								{
									context_prefixes_.emplace_back( context_prefixes_.empty()? isection->second : context_prefixes_.back() + isection->second );
									render( section_begin, section_end, recursion_limit-1 );
									context_prefixes_.pop_back();
								}
								if( find_tag( tagname ) != tag_map_.end() )
									render( section_begin, section_end, recursion_limit-1 );
								goto section_ok;
							}
							++i;
						}
						BOOST_THROW_EXCEPTION( std::runtime_error( "No closing tag for #" + tagname ) );
					section_ok:
						;
					}
					else if( x3::parse( i, end,
						start_mark_ >> *ws >> '^' >> *ws >> +(x3::char_-(ws|stop_mark_)) >> *ws >> stop_mark_,
						tagname ) )
					{
						auto const section_begin = i;
						while( i != end )
						{
							auto const section_end = i;
							if( x3::parse( i, end,
								start_mark_ >> *ws >> '/' >> *ws >> tagname >> *ws >> stop_mark_ ) )
							{
								if( find_section( tagname ) == section_map_.end() && find_tag( tagname ) == tag_map_.end() )
									render( section_begin, section_end, recursion_limit-1 );
								goto inv_section_ok;
							}
							++i;
						}
						BOOST_THROW_EXCEPTION( std::runtime_error( "No closing tag for ^" + tagname ) );
					inv_section_ok:
						;
					}
					else if( x3::parse( i, end,
						start_mark_ >> *ws >> +(x3::char_-(ws|stop_mark_)) >> *ws >> stop_mark_,
						tagname ) )
					{
						auto r = find_tag( tagname );
						if( r != tag_map_.end() )
							render( r->second.begin(), r->second.end(), recursion_limit-1 );
					}
					else
						result_ += *i++;
				}
			}
		};
	}

	template<typename t_input, typename t_tag_map, typename t_section_map>
	std::string render( t_input const & input, t_tag_map const & tag_map, t_section_map const & section_map )
	{
		detail::substitution_contex<t_tag_map, t_section_map> ctx = { tag_map, section_map, {}, "{{", "}}" };
		ctx.result_.reserve( (std::end(input)-std::begin(input))*2 );
		ctx.render( std::begin(input), std::end(input) );
		return std::move(ctx.result_);
	}

	template<typename t_input, typename t_tag_map, typename t_section_map>
	std::string render( t_input const & input, t_tag_map const & tag_map, t_section_map const & section_map, std::string const & prefix )
	{
		detail::substitution_contex<t_tag_map, t_section_map> ctx = { tag_map, section_map, { prefix }, "{{", "}}" };
		ctx.result_.reserve( (std::end(input)-std::begin(input))*2 );
		ctx.render( std::begin(input), std::end(input) );
		return std::move(ctx.result_);
	}
}
