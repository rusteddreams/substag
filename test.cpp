#include <cassert>
#include <string>
#include <map>
#include <iostream>
#include <boost/exception/diagnostic_information.hpp>

#include "substag.h"

// helper function to merge multiple consecutive '\n' characters
inline std::string merge_nl( std::string s )
{
	s.erase( std::unique( s.begin(), s.end(), []( char a, char b ) { return a == '\n' && a == b; } ), s.end() );
	return s;
}

int main()
{
	try
	{
		using namespace std;
		using namespace literals;

		// regular tags
		assert( "Hello world!" == substag::render( "Hello {{who}}"s, 
			map<string,string>{ {"who", "world{{excl}}"}, {"excl", "!"} }, 
			multimap<string,string>{} ) );

		// missing tag
		assert( "Hi!" == substag::render( "Hi{{missing}}!"s, map<string,string>{}, multimap<string,string>{} ) );

		// section
		assert( "Times: 07:23, 22:47, " == substag::render( "Times: {{#times}}{{hh}}:{{mm}}, {{/times}}"s, 
			map<string,string>{ {"t1hh", "07"}, {"t1mm", "23"}, {"t2hh", "22"}, {"t2mm", "47"} }, 
			multimap<string,string>{ {"times", "t1"}, {"times", "t2"} } ) );

		// outer context
		assert( "local/5, global/7, " == substag::render( "{{#list}}{{name}}/{{value}}, {{/list}}"s, 
			map<string,string>{ {"name", "global"}, {"1name", "local"}, {"1value", "5"}, {"2value", "7"} }, 
			multimap<string,string>{ {"list", "1"}, {"list", "2"} } ) );

		// nested sections
		assert( "(11, 12), (21, 22, 23), " == substag::render( "{{#list}}({{#array}}{{i}}{{,}}{{/array}}), {{/list}}"s, 
			map<string,string>{ 
				{"1Ai", "11"}, {"1A,",", "}, {"1Bi", "12"}, 
				{"2Ai", "21"}, {"2A,",", "}, {"2Bi", "22"}, {"2B,",", "}, {"2Ci", "23"} }, 
			multimap<string,string>{ 
				{"list", "1"}, {"list", "2"}, 
				{"1array", "A"}, {"1array", "B"}, 
				{"2array", "A"}, {"2array", "B"}, {"2array", "C"} } ) );

		// check if regular tag exists
		assert( "warning (note: not serious)!" == substag::render( "warning{{#note}} (note: {{note}}){{/note}}!"s, 
			map<string,string>{ {"note", "not serious"} }, multimap<string,string>{} ) );

		// inverted section
		assert( "Title: not provided" == substag::render( "Title: {{title}}{{^title}}not provided{{/title}}"s, 
			map<string,string>{}, multimap<string,string>{} ) );

		// comments
		assert( "Hi!" == substag::render( "Hi{{! this will be ignored }}!"s, 
			map<string,string>{}, multimap<string,string>{} ) );

		// change tag delimiters
		assert( "A  {{tag}}=A  A" == substag::render( "{{tag}} {{=<+< >+>=}} {{tag}}=<+<tag>+> <+<={{ }}=>+> {{tag}}"s, 
			map<string,string>{ {"tag", "A"} }, multimap<string,string>{} ) );

		// tag delimiters can be used inside sections
		assert( "1, 2, " == substag::render( "{{#list}}{{=[ ]=}}[i], [={{ }}=]{{/list}}"s, 
			map<string,string>{ {"1i", "1"}, {"2i", "2"}, }, 
			multimap<string,string>{ {"list", "1"}, {"list", "2"} } ) );

		// do not try to 'leak' changed delimiters from section block
		assert( "1, {{=[ ]=}}2, " == substag::render( "{{#list}}{{=[ ]=}}[i], {{/list}}"s, 
			map<string,string>{ {"1i", "1"}, {"2i", "2"}, }, 
			multimap<string,string>{ {"list", "1"}, {"list", "2"} } ) );
		// or even worse:
		try
		{
			substag::render( "{{#a}}{{=[ ]=}}[a][/a]"s, map<string,string>{ {"a", "A"}, }, multimap<string,string>{} );
		}
		catch( std::runtime_error const & e )
		{
			assert( "No closing tag for #a"s == e.what() );
		}

		// complex example
		string input_template = 
			"Hello {{name}}{{punctuation}}\n"
			"{{#colors}}\n"
			"{{! comment }} Color name: {{name}} R: {{red}} G: {{green}} B: {{blue}}{{#alpha}} A: {{alpha}}{{/alpha}}\n"
			"{{/colors}}\n"
			"{{=[[ ]]=}}\n"
			"[[^words]]\n"
			"No 'words' tag or section {{punctuation}}[[! {{punctuation}} not expanded here ]]\n"
			"[[/words]]\n"
			"[[={{ }}=]]\n"
			"{{endphrase}}\n";
		map<string, string> tags = 
		{ 
			{ "name", "World" }, 
			{ "punctuation", "!" },
			{ "endphrase", "The End{{punctuation}}" },	// recursive expansion
			{ "c1name", "Yellow" }, { "c1red", "1" }, { "c1green", "1" }, { "c1blue", "0" },
			{ "c2name", "Cyan" }, { "c2red", "0" }, { "c2green", "1" }, { "c2blue", "1" },
			{ "c3red", "1" }, { "c3green", "0" }, { "c3blue", "1" }, { "c3alpha", "0.5" }	// no 'name' tag, use 'name' from outer context
		};
		multimap<string, string> sections = 
		{
			{ "colors", "c1" },
			{ "colors", "c2" },
			{ "colors", "c3" }
		};
		cout << merge_nl( substag::render( input_template, tags, sections ) );
		/*
		Hello World!
		 Color name: Yellow R: 1 G: 1 B: 0
		 Color name: Cyan R: 0 G: 1 B: 1
		 Color name: World R: 1 G: 0 B: 1 A: 0.5
		No 'words' tag or section {{punctuation}}
		The End!
		*/
	}
	catch( ... )
	{
		std::cout << boost::current_exception_diagnostic_information();
		return 1;
	}
	return 0;
}
